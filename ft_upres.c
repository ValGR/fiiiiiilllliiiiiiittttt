/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_upres.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: flima <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/23 02:22:37 by flima        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/13 07:08:41 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"

char		**ft_upres(char **tab)
{
	char	**res;
	int		index;
	int		size_tab;

	index = 0;
	size_tab = ft_strlen(*tab) + 1;
	if (!(res = malloc(sizeof(*res) * (size_tab + 1))))
		return (NULL);
	while (index < (size_tab - 1)) // cette boucle doit prendre un max de temps
	{
		res[index] = ft_strnew(size_tab);
		res[index] = ft_strcpy(res[index], tab[index]);
		free (tab[index]);
		res[index][size_tab - 1] = '.';
		res[index][size_tab] = '\0';
		index++;
	}
	res[index] = ft_strnew(size_tab);
	index = -1;
	while (++index < size_tab)
		res[size_tab - 1][index] = '.';
	res[size_tab - 1][index] = '\0';
	res[size_tab] = NULL;
	free (tab);
	return (res);
}
