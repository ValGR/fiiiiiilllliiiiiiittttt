/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strequ.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 13:21:05 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/05 13:26:18 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	int count;

	if (s1 != '\0' && s2 != '\0')
	{
		count = ft_strcmp(s1, s2);
		if (count == 0)
			return (1);
		else
			return (0);
	}
	return (0);
}
