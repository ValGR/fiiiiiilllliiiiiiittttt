/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strncat.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 09:29:22 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/04 10:03:55 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	int lenght;
	int count;

	count = 0;
	lenght = ft_strlen(s1);
	while (s2[count] != '\0' && (size_t)count < n)
	{
		s1[lenght] = s2[count];
		count++;
		lenght++;
	}
	s1[lenght] = '\0';
	return (s1);
}
