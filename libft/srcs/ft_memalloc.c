/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memalloc.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 10:55:14 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/05 11:01:49 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	size_t	count;
	char	*memory;

	if (!(memory = (void *)malloc(sizeof(*memory) * size)))
		return (0);
	count = 0;
	while (count < size)
	{
		memory[count] = '\0';
		count++;
	}
	return ((void *)memory);
}
