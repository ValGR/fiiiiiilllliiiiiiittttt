/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_sqrt.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/17 14:15:31 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/17 15:39:55 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_sqrt(int nbr)
{
	int count;

	count = 1;
	if (nbr <= 0)
		return (0);
	while (count < nbr)
	{
		if (count * count == nbr)
			return (count);
		count++;
	}
	return (0);
}
