/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strcat.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 18:05:57 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/03 09:26:06 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *s1, const char *s2)
{
	int lenght;
	int count;

	count = 0;
	lenght = ft_strlen(s1);
	while (s2[count] != '\0')
	{
		s1[lenght] = s2[count];
		count++;
		lenght++;
	}
	s1[lenght] = '\0';
	return (s1);
}
