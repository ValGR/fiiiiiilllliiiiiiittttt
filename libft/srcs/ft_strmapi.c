/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strmapi.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 13:14:16 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/05 13:20:41 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		count;
	char	*str;

	if (s != '\0')
	{
		if (!(str = (char *)malloc(sizeof(*str) * ft_strlen(s) + 1)))
			return (0);
		count = 0;
		while (s[count] != '\0')
		{
			str[count] = f(count, s[count]);
			count++;
		}
		str[count] = '\0';
		return (str);
	}
	return (0);
}
