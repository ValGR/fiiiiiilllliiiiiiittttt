/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstmap.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/12 12:19:54 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/15 14:18:11 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*newlist;
	t_list	*first;

	if (lst == NULL || f == NULL)
		return (0);
	if (!(newlist = f(lst)))
		return (0);
	newlist->next = NULL;
	first = newlist;
	lst = lst->next;
	while (lst->next != NULL)
	{
		if (!(newlist->next = f(lst)))
			return (0);
		newlist = newlist->next;
		lst = lst->next;
	}
	if (!(newlist->next = f(lst)))
		return (0);
	return (first);
}
