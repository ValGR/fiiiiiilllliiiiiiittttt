/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_atoi.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 17:10:10 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 15:18:34 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int count;
	int number;
	int negative;

	count = 0;
	number = 0;
	negative = 1;
	while (str[count] == '\t' || str[count] == '\n'
			|| str[count] == '\r' || str[count] == '\v'
			|| str[count] == '\f' || str[count] == ' ')
		count++;
	if (str[count] == '-')
		negative *= -1;
	if (str[count] == '-')
		count++;
	if (str[count] == '+' && str[count - 1] != '-')
		negative = 1;
	if (str[count] == '+' && str[count - 1] != '-')
		count++;
	while (str[count] >= '0' && str[count] <= '9')
	{
		number *= 10;
		number += str[count++] - '0';
	}
	return (number * negative);
}
