/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strncmp.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 15:34:34 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 15:08:10 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int count;

	count = 0;
	if (n == 0)
		return (0);
	while (s1[count] == s2[count] && (s1[count] != '\0'
				|| s2[count] != '\0') && (size_t)count < n - 1)
		count++;
	return ((unsigned char)s1[count] - (unsigned char)s2[count]);
}
