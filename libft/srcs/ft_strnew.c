/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnew.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 11:14:12 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/03 16:25:30 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*str;
	size_t	count;

	if (!(str = (char *)malloc(sizeof(*str) * size + 1)))
		return (0);
	count = 0;
	while (count <= size)
	{
		str[count] = '\0';
		count++;
	}
	return (str);
}
