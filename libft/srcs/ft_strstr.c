/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strstr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 10:00:39 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/03 10:46:46 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int count;
	int rcount;

	rcount = 0;
	count = 0;
	if (ft_strlen(needle) == 0)
		return ((char *)haystack);
	while (haystack[count] != '\0')
	{
		while (haystack[count + rcount] == needle[rcount])
		{
			if (needle[rcount + 1] == '\0')
				return ((char *)haystack + count);
			rcount++;
		}
		count++;
		rcount = 0;
	}
	return (0);
}
