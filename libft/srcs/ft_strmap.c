/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strmap.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 11:59:30 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:55:17 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		count;
	char	*str;

	count = 0;
	if (s != NULL)
	{
		if (!(str = (char *)malloc(sizeof(*str) * ft_strlen(s) + 1)))
			return (0);
		while (s[count] != '\0')
		{
			str[count] = f(s[count]);
			count++;
		}
		str[count] = '\0';
		return (str);
	}
	return (0);
}
