/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strdup.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 16:26:09 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/04 10:28:17 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*str;
	int		count;

	count = ft_strlen(s1);
	if (!(str = (char *)malloc(sizeof(*str) * count + 1)))
		return (0);
	str = ft_strcpy(str, s1);
	return (str);
}
