/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlcat.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 09:41:29 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/17 12:02:40 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	len_dst;
	size_t	len_src;
	size_t	count;

	count = 0;
	len_dst = 0;
	len_src = 0;
	while (src[len_src] != '\0')
		len_src++;
	while (dst[len_dst] != '\0')
		len_dst++;
	if (len_dst > size)
		return (size + len_src);
	while (len_dst + count + 1 < size && src[count] != '\0')
	{
		dst[len_dst + count] = src[count];
		count++;
	}
	dst[len_dst + count] = '\0';
	return (len_dst + len_src);
}
