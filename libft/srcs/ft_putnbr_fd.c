/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnbr_fd.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 13:16:08 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:53:10 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	int count;

	count = n;
	if (count < 0)
	{
		ft_putchar_fd('-', fd);
		count *= -1;
	}
	if (count == -2147483648)
	{
		ft_putchar_fd('2', fd);
		count = 147483648;
	}
	if (count >= 0 && count <= 9)
		ft_putchar_fd(count + '0', fd);
	if (count >= 10)
	{
		ft_putnbr_fd(count / 10, fd);
		ft_putnbr_fd(count % 10, fd);
	}
}
