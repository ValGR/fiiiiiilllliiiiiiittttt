/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnbr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 11:08:05 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:52:49 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	int count;

	count = n;
	if (count < 0)
	{
		ft_putchar('-');
		count *= -1;
	}
	if (count == -2147483648)
	{
		ft_putchar('2');
		count = 147483648;
	}
	if (count >= 0 && count <= 9)
		ft_putchar(count + '0');
	if (count >= 10)
	{
		ft_putnbr(count / 10);
		ft_putnbr(count % 10);
	}
}
