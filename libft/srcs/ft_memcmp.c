/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memcmp.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 15:35:58 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 15:32:46 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			count;
	unsigned char	*str;
	unsigned char	*strtwo;

	if (s1 == s2 || n == 0)
		return (0);
	str = (unsigned char *)s1;
	strtwo = (unsigned char *)s2;
	count = 0;
	while (str[count] == strtwo[count] && count < n - 1)
		count++;
	return (str[count] - strtwo[count]);
}
