/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_realloc.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/16 15:22:35 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/24 16:07:49 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void **s, size_t size, size_t size_o)
{
	char	*str;

	if (!*s || size < ft_strlen(*s))
		return (NULL);
	if (!(str = malloc(sizeof(*str) * size)))
	{
		free(*s);
		*s = NULL;
		return (*s);
	}
	ft_memcpy(str, *s, size_o);
	free(*s);
	*s = str;
	return (*s);
}
