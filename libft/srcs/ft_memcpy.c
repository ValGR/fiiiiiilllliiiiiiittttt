/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memcpy.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 19:03:29 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/11 14:21:56 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	int		count;
	char	*str;
	char	*strtwo;

	count = 0;
	str = (char *)dst;
	strtwo = (char *)src;
	while ((size_t)count < n)
	{
		str[count] = strtwo[count];
		count++;
	}
	dst = str;
	return (dst);
}
