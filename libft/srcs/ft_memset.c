/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memset.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 17:31:44 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:52:03 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	int				count;
	unsigned char	*str;

	count = 0;
	str = (unsigned char *)b;
	while ((size_t)count < len)
		str[count++] = c;
	return (str);
}
