/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memmove.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 13:20:41 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 14:55:40 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	int				count;
	unsigned char	*str;
	unsigned char	*strtwo;

	str = (unsigned char *)dst;
	strtwo = (unsigned char *)src;
	count = -1;
	if (strtwo > str)
	{
		while ((size_t)(++count) < len)
			str[count] = strtwo[count];
	}
	else
	{
		count = len;
		while (--count >= 0)
			str[count] = strtwo[count];
	}
	dst = str;
	return (dst);
}
