/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memccpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 09:37:54 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/15 12:28:38 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	int				count;
	unsigned char	*str;
	unsigned char	*strtwo;

	str = (unsigned char *)dst;
	strtwo = (unsigned char *)src;
	count = 0;
	while ((size_t)count < n)
	{
		str[count] = strtwo[count];
		if (strtwo[count] == (unsigned char)c)
			return (dst + count + 1);
		count++;
	}
	return (0);
}
