/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strsplit.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 13:20:00 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/16 11:50:03 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_count_word(char const *s, char c)
{
	int		count;
	int		len;
	int		b;

	len = 0;
	count = 0;
	b = 1;
	while (s[count] != '\0')
	{
		if (b == 0 && s[count] == c)
			b = 1;
		if (b == 1 && s[count] != c)
		{
			len++;
			b = 0;
		}
		count++;
	}
	return (len);
}

static	char	*ft_wordcomplete(char const *s, char c, int *count)
{
	char	*st;
	int		len;

	len = 0;
	while (s[*count] == c && s[*count] != '\0')
		*count += 1;
	while (s[*count + len] != c)
	{
		len++;
		if (s[*count + len] == '\0')
			break ;
	}
	if (!(st = (char *)malloc(sizeof(*st) * len + 1)))
		return (0);
	len = 0;
	while (s[*count] != c && s[*count] != '\0')
	{
		st[len++] = s[*count];
		*count += 1;
	}
	st[len] = '\0';
	return (st);
}

char			**ft_strsplit(char const *s, char c)
{
	int		words;
	char	**str;
	int		count;
	int		len_word;

	if (s != '\0')
	{
		count = 0;
		len_word = 0;
		words = ft_count_word(s, c);
		if (!(str = (char **)malloc(sizeof(*str) * words + 1)))
			return (0);
		if (len_word == words && s[count] != '\0')
			if (s[count] != c)
				str[len_word++] = ft_wordcomplete(s, c, &count);
		while (len_word < words && s[count] != '\0')
		{
			if (s[count] != c)
				str[len_word++] = ft_wordcomplete(s, c, &count);
			count++;
		}
		str[len_word] = NULL;
		return (str);
	}
	return (NULL);
}
