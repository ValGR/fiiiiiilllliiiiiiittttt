/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strsub.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 13:35:49 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:56:43 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char			*str;
	unsigned int	count;

	count = 0;
	if (s != '\0')
	{
		if (!(str = (char *)malloc(sizeof(*str) * (len + 1))))
			return (0);
		while (count < len)
		{
			str[count++] = s[start];
			start++;
		}
		str[count] = '\0';
		return (str);
	}
	return (0);
}
