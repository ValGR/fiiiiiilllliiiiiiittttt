/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strrchr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 15:04:27 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/03 15:08:17 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int count;
	int savedcount;

	count = 0;
	savedcount = -1;
	while (s[count] != '\0')
	{
		if (s[count] == c)
			savedcount = count;
		count++;
	}
	if (s[count] == c)
		savedcount = count;
	if (savedcount == -1)
		return (0);
	else
		return ((char *)s + savedcount);
}
