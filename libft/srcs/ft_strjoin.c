/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strjoin.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 14:00:33 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/11/02 14:17:56 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		lens1;
	int		lens2;
	char	*str;

	lens1 = 0;
	lens2 = 0;
	if (s1 != '\0' && s2 != '\0')
	{
		while (s1[lens1] != '\0')
			lens1++;
		while (s2[lens2] != '\0')
			lens2++;
		if (!(str = (char *)malloc(sizeof(*str) * (lens1 + lens2 + 1))))
			return (0);
		lens2 = 0;
		lens1 = -1;
		while (s1[++lens1] != '\0')
			str[lens1] = s1[lens1];
		while (s2[lens2] != '\0')
			str[lens1++] = s2[lens2++];
		str[lens1] = '\0';
		return (str);
	}
	return (0);
}
