/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strtrim.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/05 14:26:31 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:57:09 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		count;
	int		start;
	int		len;
	char	*str;

	start = 0;
	count = 0;
	if (s == '\0')
		return (0);
	len = ft_strlen(s) - 1;
	while (s[start] != '\0' && (s[start] == ' ' || s[start] == '\n'
				|| s[start] == '\t'))
		start++;
	while (len > start && (s[len] == ' ' || s[len] == '\n' || s[len] == '\t'))
		len--;
	if (!(str = (char *)malloc(sizeof(*str) * (len - start + 2))))
		return (0);
	count = -1;
	while (++count <= len - start)
		str[count] = s[start + count];
	str[count] = '\0';
	return (str);
}
