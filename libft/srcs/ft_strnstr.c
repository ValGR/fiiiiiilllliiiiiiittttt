/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnstr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 10:20:43 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 14:56:18 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	int count;
	int rcount;

	count = 0;
	rcount = 0;
	if (ft_strlen(needle) == 0)
		return ((char *)haystack);
	while (haystack[count] != '\0')
	{
		while (haystack[count + rcount] == needle[rcount]
				&& (size_t)count + rcount < len)
		{
			if (needle[rcount + 1] == '\0')
				return ((char *)haystack + count);
			rcount++;
		}
		count++;
		rcount = 0;
	}
	return (0);
}
