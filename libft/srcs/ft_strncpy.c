/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strncpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 16:54:02 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/04 10:38:54 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	int count;

	count = 0;
	while (src[count] != '\0' && (size_t)count < len)
	{
		dst[count] = src[count];
		count++;
	}
	while ((size_t)count < len)
	{
		dst[count] = '\0';
		count++;
	}
	return (dst);
}
