/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_itoa.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/09 16:59:47 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/11 11:41:19 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_count_digit(int nbr)
{
	int		count;

	count = 0;
	if (nbr < 0)
	{
		count++;
		nbr = -nbr;
	}
	while (nbr /= 10)
		count++;
	count++;
	return (count);
}

char			*ft_itoa(int n)
{
	char	*str;
	int		nbr;
	int		count;
	int		neg;

	if (!(str = (char *)malloc(sizeof(*str) * ft_count_digit(n) + 1)))
		return (0);
	nbr = n;
	neg = 1;
	count = ft_count_digit(n);
	if (nbr == -2147483648)
		return (ft_strdup("-2147483648"));
	if (nbr < 0)
		neg = -1;
	if (nbr < 0)
		nbr = -nbr;
	str[count--] = '\0';
	while (count >= 0)
	{
		str[count--] = nbr % 10 + '0';
		nbr /= 10;
	}
	if (neg < 0)
		str[0] = '-';
	return (str);
}
