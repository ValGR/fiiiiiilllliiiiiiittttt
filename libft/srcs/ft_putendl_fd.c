/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putendl_fd.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 13:07:58 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/10/08 13:14:34 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_fd(char const *s, int fd)
{
	int count;

	if (s != '\0')
	{
		count = 0;
		while (s[count] != '\0')
			ft_putchar_fd(s[count++], fd);
		ft_putchar_fd('\n', fd);
	}
}
