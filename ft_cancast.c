/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cancast.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: flima <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/12 17:38:29 by flima        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/13 07:26:45 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"

static void			ft_resettetri(char **royal)
{
	int index;
	int jndex;

	index = 0;
	jndex = 0;
	while (royal[jndex])
	{
		index = 0;
		while (royal[jndex][index])
		{
			royal[jndex][index] = '.';
			index++;
		}
		jndex++;
	}
}

static int			ft_casttetriminos(int res_x, int res_y, t_tetri *tetri, char **res)
{
	int index;

	index = 0;
	while (index < 4)
	{
		if (res[res_y + tetri->points[index][Y]] == NULL)
			return (0);
		if (res[res_y + tetri->points[index][Y]][res_x + tetri->points[index][X]] != '.')
		{
			return (0);
		}
		res[res_y + tetri->points[index][Y]][res_x + tetri->points[index][X]] = tetri->letter;
		index++;
	}
	return (1);
}

int					ft_cancast(char **res, t_tetri **tetri, int nbr_tetri)
{
	int		index;

	index = 0;
	while (index < nbr_tetri)
	{
		if (!(ft_casttetriminos(tetri[index]->res_x, tetri[index]->res_y, tetri[index], res)))
		{
			index = -1;
			ft_resettetri(res);
			return (0);
		}
		index++;
	}
	return (1);
}
