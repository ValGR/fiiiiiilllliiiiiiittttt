/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_store_tetriminos.c                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/20 15:12:40 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/05 03:29:37 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

void	ft_checkmax(int nbr_tetri, t_tetri **tetri)
{
	int		index;
	int		max_x;
	int		max_y;

	index = 0;
	max_x = 0;
	max_y = 0;
	while (index < 4)
	{
		if (tetri[nbr_tetri]->points[index][X] > max_x)
			max_x = tetri[nbr_tetri]->points[index][X];
		if (tetri[nbr_tetri]->points[index][Y] > max_y)
			max_y = tetri[nbr_tetri]->points[index][Y];
		index++;
	}
	tetri[nbr_tetri]->max_x = max_x;
	tetri[nbr_tetri]->max_y = max_y;
}

// int index = l'index sur 5 = index % 5 / jindex = jindex / tetri = adresse du tableau de tetri / nbr tetri = sur quelle tetri on se trouve = index / 5
void	ft_store_tetriminos(int index, int jindex, t_tetri **tetri, int nbr_tetri)
{
	int min_x;
	int min_y;

	min_x = 4;
	min_y = 4;
	// REGARDE LA LIGNE DU DESSUS STP ET VA TE COUCHER BOLOSS
	tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][X] = jindex;
	tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][Y] = index;
	tetri[nbr_tetri]->count++;
	if (tetri[nbr_tetri]->count == 4)
	{
		tetri[nbr_tetri]->count = -1;
		while (++tetri[nbr_tetri]->count < 4)
		{
			if (tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][X] < min_x)
				min_x = tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][X];
		   	if (tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][Y] < min_y)
				min_y = tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][Y];
		}
		tetri[nbr_tetri]->count = -1;
		while (++tetri[nbr_tetri]->count < 4)
		{
			tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][X] -= min_x;
			tetri[nbr_tetri]->points[tetri[nbr_tetri]->count][Y] -= min_y;
		}
		ft_checkmax(nbr_tetri, tetri);
	}
}
