/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_sqrt_rounded.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/05 02:24:14 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/12 17:31:50 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int		ft_sqrt_rounded(int nbr)
{
	int		count;

	count = 0;
	if (nbr <= 0)
		return (0);
	while (count <= nbr)
	{
		if (count * count == nbr)
			return (count + 1);
		else if (count * count > nbr)
			return (count);
		count++;
	}
	return (0);
}
