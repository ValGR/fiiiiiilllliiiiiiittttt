/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fillit.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/20 16:23:10 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/12 21:13:50 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef	FILLIT_H

# define FILLIT_H
# define X 0
# define Y 1

# include "libft/includes/libft.h"
# include "get_next_line.h"

typedef	struct		s_tetri
{
	int				points[4][2];
	int				count;
	int				max_x;
	int				max_y;
	int				res_x;
	int				res_y;
	int				save;
	char			letter;
}					t_tetri;

void				ft_store_tetriminos(int index, int jindex, t_tetri **tetri, int nbr_tetri);
char				**ft_tetriorder(t_tetri **tetri, int nbr_tetri);
char				**ft_createakingdoms(int nbr_tetri);
int					ft_cancast(char **res, t_tetri **tetri, int nbr_tetri);
char				**ft_upres(char **tab);
int					ft_sqrt_rounded(int nbr);


#endif
