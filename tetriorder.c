/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tetriorder.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: flima <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/27 01:15:23 by flima        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/13 07:13:08 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"

static void			ft_initialisation(t_tetri **tetri)
{
	int index;

	index = 0;
	tetri[0]->save = 0;
	while (index < 26)
	{
		tetri[index]->res_x = 0;
		tetri[index]->res_y = 0;
		index++;
	}
}

static void			ft_resurection(char **royal, t_tetri **tetri, int nbr_tetri)
{
	int theend;

	theend = 1;
	nbr_tetri--;
	while (theend)
	{
	//	ft_putendl("oui");
		tetri[nbr_tetri]->res_x++;
		if (royal[tetri[nbr_tetri]->res_y][tetri[nbr_tetri]->res_x] == '\0')
		{
			tetri[nbr_tetri]->res_y++;
			tetri[nbr_tetri]->res_x = 0;
			if (royal[tetri[nbr_tetri]->res_y] == NULL && tetri[nbr_tetri]->letter != 'A' && royal[tetri[nbr_tetri - 1]->res_y] != NULL)
				tetri[nbr_tetri]->res_y = 0;
			else
				theend = 0;
		}
		else
			theend = 0;
		nbr_tetri--;
	}
}

static int				ft_endofthegod(t_tetri **tetri, char **royal, int count)
{
	int index;

	index = 0;
	count--;
	while (index < count)
	{
		if (royal[tetri[index]->res_y] != NULL)
			return (1);
		index++;
	}
	return (0);
}

static int				ft_ending(char **royal)
{
	int		index;

	index = 0;
	while (royal[0][index])
	{
		if (royal[0][index] <= 'Z' && royal[0][index] >= 'A')
			return (0);
		index++;
	}
	return (1);
}

char			**ft_tetriorder(t_tetri **tetri, int nbr_tetri)
{
	char	**royal;
	int		valid;
	int		index;

	valid = 0;
	index = -1;
//	ft_putendl("ft_createakingdoms");
	royal = ft_createakingdoms(nbr_tetri);
//	ft_putendl("->ok");
//	while (royal[++index])
//		ft_putendl(royal[index]);
	while (ft_ending(royal))
	{
	//	ft_putendl("ft_initialisation");
		ft_initialisation(tetri);
	//	ft_putendl("->ok");
//		ft_putendl("endofthegod || cancast");
		while (ft_endofthegod(tetri, royal, nbr_tetri) && !(valid = ft_cancast(royal, tetri, nbr_tetri)))
		{
	//		ft_putstr("resurection");
			ft_resurection(royal, tetri, nbr_tetri);
	//		ft_putendl("->ok");
		}
	//	ft_putendl("->ok");
		if (valid == 0)
			royal = ft_upres(royal);
	}
	return (royal);
}
