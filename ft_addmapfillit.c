/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_addmapfillit.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: flima <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 02:08:59 by flima        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/13 06:37:30 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"

/*
**ft_trycast
**Renvoit
** _-1_		Pas assez de place
** _0_		# sur un #
** _1_ 		gg
*/

static int		ft_trycast(int res_x, int res_y, t_tetri *tetri, char **res)
{
	int index;

	index = 0;
	while (index < 4)
	{
		if (res[res_y + tetri->points[index][Y]] == NULL)
			return (0);
		if (res[res_y + tetri->points[index][Y]][res_x + tetri->points[index][X]] != '.') 
			return (0);
		index++;
	}
	return (1);
}


/*
**ft_casttetriminos
**copie le tetriminos
**dans le tableau a double entree
*/
static void			ft_casttetriminos(int res_x, int res_y, t_tetri *tetri, char **res)
{
	int index;

	index = 0;
	while (index < 4)
	{
		res[res_y + tetri->points[index][Y]][res_x + tetri->points[index][X]] = tetri->letter;
		index++;
	}
}

char			**ft_addmapfillit(t_tetri **tetri, int size_tab, char **res)
{
	
	return (res);
}
