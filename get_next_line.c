/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_next_line.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/23 14:06:22 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 12:12:15 by vgras       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

static	int		ft_readline(const int fd, t_var *v)
{
	int			ret;

	if ((ret = read(fd, v->sstr, BUFF_SIZE)) > 0)
	{
		v->sstr[ret] = '\0';
		return (1);
	}
	if (ret < 0)
		return (-1);
	return (0);
}

static	int		ft_detect_linefeed(t_var *v)
{
	int			count;
	int			ret;

	ret = 0;
	v->startcut = v->start;
	count = v->start;
	while (v->sstr[count] != '\n' && v->sstr[count] != '\0')
		count++;
	if (v->sstr[count] == '\n')
	{
		v->len = count - v->startcut;
		v->start = count + 1;
		return (1);
	}
	v->len = count - v->start;
	v->start = 0;
	return (0);
}

static	void	ft_subline(char **line, t_var *v)
{
	char	*tmp;
	char	*test;

	tmp = ft_strdup(*line);
	free(*line);
	test = ft_strsub(v->sstr, v->startcut, v->len);
	*line = ft_strjoin(tmp, test);
	free(test);
	free(tmp);
}

static	int		ft_check_line(const int fd, char **line, t_var *v)
{
	int			ret;

	while (ft_detect_linefeed(v) == 0)
	{
		ft_subline(line, v);
		if ((ret = ft_readline(fd, v) == 0))
		{
			if (v->sstr[v->startcut] == '\0')
			{
				ft_strdel(line);
				ft_strdel(&(v->sstr));
				return (0);
			}
			ft_strdel(&(v->sstr));
			return (1);
		}
	}
	if (ret < 0)
		return (-1);
	return (2);
}

int				get_next_line(const int fd, char **line)
{
	static t_var	v[6000];
	int				ret;

	if (fd < 0 || !line || read(fd, 0, 0) < 0 || BUFF_SIZE < 1)
		return (-1);
	if (!v[fd].sstr)
	{
		v[fd].sstr = ft_memalloc(BUFF_SIZE + 1);
		if ((ret = ft_readline(fd, &v[fd])) == 0)
			return (0);
	}
	*line = ft_memalloc(1);
	if ((ret = ft_check_line(fd, line, &v[fd])) != 2)
		return (ret);
	ft_subline(line, &v[fd]);
	if (ret < 0)
		return (-1);
	return (1);
}
