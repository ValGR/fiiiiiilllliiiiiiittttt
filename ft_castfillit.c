/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_castfillit.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: flima <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 01:40:07 by flima        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/13 07:26:14 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"

static char		**ft_createnewmap(int min_size)
{
	char	**ret;
	int		index;
	int		sub_index;

	index = 0;
	if (!(ret = malloc(sizeof(*ret) * (min_size + 3))))
		return (NULL);
	while (index < min_size)
	{
		ret[index] = ft_strnew(min_size + 3);
		sub_index = 0;
		while (sub_index < min_size)
			ret[index][sub_index++] = '.';
		ret[index][sub_index] = '\0';
		index++;
	}
	ret[index] = NULL;
	return (ret);
}

char			**ft_createakingdoms(int nbr_tetri)
{
	char	**res;
	int		size_tab;

	size_tab = ft_sqrt_rounded(nbr_tetri * 4);
	res = ft_createnewmap(size_tab - 1);
	return (res);
}
