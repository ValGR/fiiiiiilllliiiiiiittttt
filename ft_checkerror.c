/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_checkerror.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vgras <marvin@le-101.fr>                   +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/19 14:01:09 by vgras        #+#   ##    ##    #+#       */
/*   Updated: 2018/12/12 01:05:51 by flima       ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "fillit.h"
#include <stdio.h>

static	int		ft_check_dieze(int index, int jindex, char **line, t_tetri **tetri)
{
	if (line[index][jindex] == '#')
	{
		if (index >= 5)
			ft_store_tetriminos(index % 5, jindex, tetri, index / 5);
		else
			ft_store_tetriminos(index, jindex, tetri, 0);
		if (line[index][jindex + 1] == '#' || 
			line[index][jindex - 1] == '#' || line[index + 1][jindex] == '#')
			return (1);
		else if (index > 0 && line[index - 1][jindex] == '#')
			return (1);
		return (0);
	}
	return (2);
}

static	int		ft_lastcheck(int index, int jndex, char **tetris, int dieze_count)
{
	if ((index) % 5 == 0)
		if (dieze_count != 4)
			return (0);
	if (tetris[index - 1][jndex] != '\0')
		return (0);
	return (1);
}

static	int     ft_verefsize(char **tetris, t_tetri **tetri)
{
	int index;
	int jndex;
	int	dieze_count;

	index = 0;
	dieze_count = 0;
//	ft_putstr("1");
	while (tetris[index] != NULL)
	{
		jndex = 0;
		while ((jndex < 4) && ((index + 1) % 5) != 0)
		{
			if (tetris[index][jndex] != '.' && tetris[index][jndex] != '#' && tetris[index][jndex] != '\0')
				return (0);
			else if(tetris[index][jndex] == '#')
				dieze_count++;
			if (ft_check_dieze(index, jndex++, tetris, tetri) == 0)
				return (0);
		}
	//	if (ft_lastcheck(++index, jndex, tetris, dieze_count) == 0)
	//		return (0);
		index++;
		if (dieze_count == 4 && (index) % 5 == 0)
			dieze_count = 0;
	}
	return (index / 5);
}

static void		ft_giveaalpha(t_tetri **tetri)
{
	int index;

	index = 0;
	while (index < 26)
	{
		tetri[index]->letter = 'A' + index;
		index++;
	}
}

static	int     ft_verefillit(char **tetris)
{
	int count;
	t_tetri	**tetri;
	int index;
	char **royal;

	count = -1;
	tetri = malloc(sizeof(t_tetri) * 26 + 1); // il en a rien a foutre de mon malloc // Alloue le tableau de struct
	while (++count < 26)
		tetri[count] = malloc(sizeof(t_tetri)); // Alloue chaque struct
	count = 0;
	while (count < 26)
		tetri[count++]->count = 0;
	if (!(index = ft_verefsize(tetris, tetri))) // Comme une struct est une adresse, pas besoin d'envoyer d'adresse, car c'est en soit deja une adresse
		return (0);
	ft_giveaalpha(tetri);
	royal = ft_tetriorder(tetri, index);
	index = 0;
	while (royal[index])
	{
		ft_putendl(royal[index]);
		index++;
	}
	free (royal);
	count = -1;
	while (++count < 26)
		free(tetri[count]);
	free (tetri);
	return (1);
}

int		nbr_char(char *tetri)
{
	int 	index;

	index = 0;
	while (tetri[index] != '\0')
	{
		index++;
		if (index > 4)
			return (0);
	}
	return (1);
}

int     main(int argc, char **argv)
{
	int     fd;
	int     index;
	char    *line;
	char    **tab;

	index = 0;
	if(!(tab = malloc(sizeof(char*) * (250))))
		return (0);
	fd = open(argv[1], O_RDONLY);
	if (argc == 2)
	{
		while (get_next_line(fd, &line) > 0)
		{
			// il y a un probleme sur la facon de remplir les index. la taille des indexs depend de la taille du buffer
			tab[index] = ft_strdup(line);
			free(line);
			if (nbr_char(tab[index]) == 0)
			{
				ft_putstr("error");
				return (0);
			}
		//	ft_putstr(tab[index]);
			index++;
		//	ft_putnbr(index);
		}
		tab[index] = NULL;
		if (ft_verefillit(tab) == 0)
		{
			ft_putstr("error");
			return (0);
		}
		index = -1;
		while (++index < 250)
			free (tab[index]);
		free (tab);
	}
	close(fd);
	return (0);
}
